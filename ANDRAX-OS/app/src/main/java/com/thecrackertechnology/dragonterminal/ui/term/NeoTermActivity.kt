package com.thecrackertechnology.dragonterminal.ui.term

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.content.ContextCompat
import android.support.v4.view.OnApplyWindowInsetsListener
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.ImageButton
import android.widget.Toast
import com.thecrackertechnology.andrax.AndraxApp
import com.thecrackertechnology.andrax.R
import com.thecrackertechnology.andrax.SplashActivity
import com.thecrackertechnology.dragonterminal.backend.TerminalSession
import com.thecrackertechnology.dragonterminal.component.profile.ProfileComponent
import com.thecrackertechnology.dragonterminal.frontend.component.ComponentManager
import com.thecrackertechnology.dragonterminal.frontend.config.NeoPermission
import com.thecrackertechnology.dragonterminal.frontend.config.NeoPreference
import com.thecrackertechnology.dragonterminal.frontend.session.shell.ShellParameter
import com.thecrackertechnology.dragonterminal.frontend.session.shell.ShellProfile
import com.thecrackertechnology.dragonterminal.frontend.session.shell.client.TermSessionCallback
import com.thecrackertechnology.dragonterminal.frontend.session.shell.client.TermViewClient
import com.thecrackertechnology.dragonterminal.frontend.session.shell.client.event.*
import com.thecrackertechnology.dragonterminal.frontend.session.xorg.XParameter
import com.thecrackertechnology.dragonterminal.frontend.session.xorg.XSession
import com.thecrackertechnology.dragonterminal.services.NeoTermService
import com.thecrackertechnology.dragonterminal.ui.settings.SettingActivity
import com.thecrackertechnology.dragonterminal.ui.term.tab.NeoTab
import com.thecrackertechnology.dragonterminal.ui.term.tab.NeoTabDecorator
import com.thecrackertechnology.dragonterminal.ui.term.tab.TermTab
import com.thecrackertechnology.dragonterminal.ui.term.tab.XSessionTab
import com.thecrackertechnology.dragonterminal.utils.AssetsUtils
import com.thecrackertechnology.dragonterminal.utils.FullScreenHelper
import com.thecrackertechnology.dragonterminal.utils.RangedInt
import de.mrapp.android.tabswitcher.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.io.*
import java.lang.Process
import java.nio.charset.Charset
import java.util.ArrayList


class NeoTermActivity : AppCompatActivity(), ServiceConnection, SharedPreferences.OnSharedPreferenceChangeListener {
    companion object {
        const val KEY_NO_RESTORE = "no_restore"
        const val REQUEST_SETUP = 22313
    }

    private lateinit var errorDialog: Dialog

    lateinit var tabSwitcher: TabSwitcher
    private lateinit var fullScreenHelper: FullScreenHelper
    lateinit var toolbar: Toolbar

    var addSessionListener = createAddSessionListener()
    private var termService: NeoTermService? = null

    val fullscreen = NeoPreference.isFullScreenEnabled()
    var tshow = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)



        /**Handler().postDelayed({

            if (tabSwitcher.count > 0) {

                checkinstallterm()

            }

        }, 800) **/


        changehostname("ANDRAX-Hackers-Platform")

        isRooted(this)

        NeoPermission.initAppPermission(this, NeoPermission.REQUEST_APP_PERMISSION)

        if (fullscreen) {
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        }

        val SDCARD_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1

        if (ContextCompat.checkSelfPermission(
                        this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    SDCARD_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE
            )
        }

        setContentView(R.layout.ui_main)

        toolbar = findViewById(R.id.terminal_toolbar)
        setSupportActionBar(toolbar)

        fullScreenHelper = FullScreenHelper.injectActivity(this, fullscreen, peekRecreating())
        fullScreenHelper.setKeyBoardListener(object : FullScreenHelper.KeyBoardListener {
            override fun onKeyboardChange(isShow: Boolean, keyboardHeight: Int) {
                if (tabSwitcher.selectedTab is TermTab) {
                    val tab = tabSwitcher.selectedTab as TermTab
                    // isShow -> toolbarHide
                    toggleToolbar(tab.toolbar, !isShow)
                }
            }
        })

        tabSwitcher = findViewById(R.id.tab_switcher)
        tabSwitcher.decorator = NeoTabDecorator(this)
        ViewCompat.setOnApplyWindowInsetsListener(tabSwitcher, createWindowInsetsListener())
        tabSwitcher.showToolbars(false)

        val serviceIntent = Intent(this, NeoTermService::class.java)
        startService(serviceIntent)
        bindService(serviceIntent, this, 0)

        if (savedInstanceState == null) {
            val extras = intent.extras
            if (extras != null) {

                val method = extras.getString("recfromshort")
                if (method == "recfromshortcut") {

                    NeoPreference.setLoginShellName("/system/bin/sh")

                    Handler().postDelayed({

                        if (tabSwitcher.count > 0) {

                            NeoPreference.setLoginShellName(AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")

                        }

                    }, 1000)

                } else {
                    NeoPreference.setLoginShellName(AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")
                }
            }

        }

    }

    private fun toggleToolbar(toolbar: Toolbar?, visible: Boolean) {
        if (toolbar == null) {
            return
        }

        if (NeoPreference.isFullScreenEnabled() || NeoPreference.isHideToolbarEnabled()) {
            val toolbarHeight = toolbar.height.toFloat()
            val translationY = if (visible) 0.toFloat() else -toolbarHeight
            if (visible) {
                toolbar.visibility = View.VISIBLE
                toolbar.animate()
                        .translationY(translationY)
                        .start()
                tshow = true
            } else {
                toolbar.animate()
                        .translationY(translationY)
                        .withEndAction {
                            toolbar.visibility = View.GONE
                        }
                        .start()
                tshow = false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        TabSwitcher.setupWithMenu(tabSwitcher, toolbar.menu, {
            if (!tabSwitcher.isSwitcherShown) {
                val imm = this@NeoTermActivity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                if (imm.isActive && tabSwitcher.selectedTab is TermTab) {
                    val tab = tabSwitcher.selectedTab as TermTab
                    tab.requireHideIme()
                }
                toggleSwitcher(showSwitcher = true, easterEgg = true)
            } else {
                toggleSwitcher(showSwitcher = false, easterEgg = true)
            }
        })
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.menu_item_settings -> {
                startActivity(Intent(this, SettingActivity::class.java))
                true
            }


            R.id.menu_item_recovery -> {

                val oldshell = NeoPreference.getLoginShellName()

                NeoPreference.setLoginShellName("/system/bin/sh")

                addNewSession()

                NeoPreference.setLoginShellName(oldshell)

                true
            }

            R.id.menu_item_howtohack -> {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://learnapp.thecrackertechnology.com/advanced-hacking-training/")))
                true
            }

            R.id.menu_item_umount -> {

                val umountsdcard = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/busybox umount " + AndraxApp.get().applicationInfo.dataDir + "/ANDRAX/sdcard")
                umountsdcard.waitFor()

                val umountcore = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/busybox umount -lf " + AndraxApp.get().applicationInfo.dataDir + "/ANDRAX")
                umountcore.waitFor()

                finish()

                true
            }

            R.id.menu_item_mountsdcard -> {

                val mkdirmountsdcard = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/busybox mkdir " + AndraxApp.get().applicationInfo.dataDir + "/ANDRAX/sdcard")
                mkdirmountsdcard.waitFor()

                val mountsdcard = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/busybox mount -o bind /sdcard " + AndraxApp.get().applicationInfo.dataDir + "/ANDRAX/sdcard")
                mountsdcard.waitFor()

                true
            }

            R.id.menu_item_umountsdcard -> {

                val umountsdcard = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/busybox umount " + AndraxApp.get().applicationInfo.dataDir + "/ANDRAX/sdcard")
                umountsdcard.waitFor()

                true
            }

            R.id.menu_item_logcat -> {

                val umountcore = Runtime.getRuntime().exec("su -c dmesg > /sdcard/Download/dmesg.txt")
                umountcore.waitFor()

                finish()

                true
            }

            R.id.andraxshellitem -> {

                val rmandraxshell = Runtime.getRuntime().exec("su -c rm -rf " + AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")
                rmandraxshell.waitFor()

                val cpandraxshell = Runtime.getRuntime().exec("su -c cp -rf " + AndraxApp.get().filesDir.absolutePath + "/bin/andraxshelltmp " + AndraxApp.get().filesDir + "/bin/andraxshell.sh")
                cpandraxshell.waitFor()

                val chmodandraxshell = Runtime.getRuntime().exec("su -c chmod 777 " + AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")
                chmodandraxshell.waitFor()

                addNewSession()

                true
            }

            R.id.rootshellitem -> {

                val rmandraxshell = Runtime.getRuntime().exec("su -c rm -rf " + AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")
                rmandraxshell.waitFor()

                val cprootshell = Runtime.getRuntime().exec("su -c cp -rf " + AndraxApp.get().filesDir.absolutePath + "/bin/rootshell " + AndraxApp.get().filesDir + "/bin/andraxshell.sh")
                cprootshell.waitFor()

                val chmodandraxshell = Runtime.getRuntime().exec("su -c chmod 777 " + AndraxApp.get().filesDir.absolutePath + "/bin/andraxshell.sh")
                chmodandraxshell.waitFor()

                addNewSession()

                true
            }

            R.id.menu_item_new_session -> {
                addNewSession()
                true
            }

            R.id.dco_information -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Information_Gathering")))
                true
            }

            R.id.dco_scanning -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Scanning")))
                true
            }

            R.id.dco_packet -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Packet_Crafting")))
                true
            }

            R.id.dco_network -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_network_hacking")))
                true
            }

            R.id.dco_website -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_website_hacking")))
                true
            }

            R.id.dco_password -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Password_Hacking")))
                true
            }

            R.id.dco_wireless -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Wireless_Hacking")))
                true
            }

            R.id.dco_exploitation -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_exploitation")))
                true
            }

            R.id.dco_stress -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_stress_testing")))
                true
            }

            R.id.dco_phishing -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_phishing")))
                true
            }

            R.id.dco_voip -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_voip_3g_4g")))
                true
            }

            R.id.dco_ics_scada -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_ics_scada_iot")))
                true
            }

            R.id.dco_mainframes -> {
                startActivity(Intent(this, Class.forName("com.thecrackertechnology.andrax.Dco_Mainframes")))
                true
            }



            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onPause() {
        super.onPause()
        val tab = tabSwitcher.selectedTab as NeoTab?
        tab?.onPause()
    }

    override fun onResume() {
        super.onResume()

        try {

            PreferenceManager.getDefaultSharedPreferences(this)
                    .registerOnSharedPreferenceChangeListener(this)
            tabSwitcher.addListener(object : TabSwitcherListener {
                override fun onSwitcherShown(tabSwitcher: TabSwitcher) {
                    toolbar.setNavigationIcon(R.drawable.ic_add_box_white_24dp)
                    toolbar.setNavigationOnClickListener(addSessionListener)
                    toolbar.setBackgroundResource(android.R.color.transparent)
                    toolbar.animate().alpha(0f).setDuration(300).withEndAction {
                        toolbar.alpha = 1f
                    }.start()
                }

                override fun onSwitcherHidden(tabSwitcher: TabSwitcher) {
                    toolbar.navigationIcon = null
                    toolbar.setNavigationOnClickListener(null)
                    toolbar.setBackgroundResource(R.color.black_fuck)
                }

                override fun onSelectionChanged(tabSwitcher: TabSwitcher, selectedTabIndex: Int, selectedTab: Tab?) {
                    if (selectedTab is TermTab && selectedTab.termData.termSession != null) {
                        NeoPreference.storeCurrentSession(selectedTab.termData.termSession!!)
                    }
                }

                override fun onTabAdded(tabSwitcher: TabSwitcher, index: Int, tab: Tab, animation: Animation) {
                    update_colors()
                }

                override fun onTabRemoved(tabSwitcher: TabSwitcher, index: Int, tab: Tab, animation: Animation) {
                    if (tab is TermTab) {
                        SessionRemover.removeSession(termService, tab)
                    } else if (tab is XSessionTab) {
                        SessionRemover.removeXSession(termService, tab)
                    }
                }

                override fun onAllTabsRemoved(tabSwitcher: TabSwitcher, tabs: Array<out Tab>, animation: Animation) {
                }
            })
            val tab = tabSwitcher.selectedTab as NeoTab?
            tab?.onResume()

        } catch (e: Exception) {

        }


    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        val tab = tabSwitcher.selectedTab as NeoTab?
        tab?.onStart()
    }

    override fun onStop() {
        super.onStop()
        // After stopped, window locations may changed
        // Rebind it at next time.
        forEachTab<TermTab> { it.resetAutoCompleteStatus() }
        val tab = tabSwitcher.selectedTab as NeoTab?
        tab?.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        val tab = tabSwitcher.selectedTab as NeoTab?
        tab?.onDestroy()
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this)

        if (termService != null) {
            if (termService!!.sessions.isEmpty()) {
                termService!!.stopSelf()
            }
            termService = null
        }
        unbindService(this)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        val tab = tabSwitcher.selectedTab as NeoTab?
        tab?.onWindowFocusChanged(hasFocus)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        when (keyCode) {
            KeyEvent.KEYCODE_BACK -> {
                if (event?.action == KeyEvent.ACTION_DOWN && tabSwitcher.isSwitcherShown && tabSwitcher.count > 0) {
                    toggleSwitcher(showSwitcher = false, easterEgg = false)
                    return true
                }
            }
            KeyEvent.KEYCODE_MENU -> {
                if (toolbar.isOverflowMenuShowing) {
                    toolbar.hideOverflowMenu()
                } else {
                    toolbar.showOverflowMenu()
                }
                return true
            }
        }
        return super.onKeyDown(keyCode, event)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            NeoPermission.REQUEST_APP_PERMISSION -> {
                if (grantResults.isEmpty()
                        || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    AlertDialog.Builder(this).setMessage(R.string.permission_denied)
                            .setPositiveButton(android.R.string.ok, { _: DialogInterface, _: Int ->
                                finish()
                            })
                            .show()
                }
                return
            }
        }
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        if (key == getString(R.string.key_ui_fullscreen)) {
            setFullScreenMode(NeoPreference.isFullScreenEnabled())
        } else if (key == getString(R.string.key_customization_color_scheme)) {
            if (tabSwitcher.count > 0) {
                val tab = tabSwitcher.selectedTab
                if (tab is TermTab) {
                    tab.updateColorScheme()
                }
            }
        }
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        if (termService != null) {
            finish()
        }
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        termService = (service as NeoTermService.NeoTermBinder).service
        if (termService == null) {
            finish()
            return
        }

        if (!isRecreating()) {

            var reader: BufferedReader? = null
            var testmsg=""
            var result = false
            var stdin: OutputStream? = null
            val stdout: InputStream
            val params = ArrayList<String>()

            try {
                val pb = ProcessBuilder("su")
                pb.directory(File(AndraxApp.get().applicationInfo.dataDir))
                pb.redirectErrorStream(false)
                val process: Process = pb.start()
                stdin = process.outputStream
                stdout = process.inputStream
                params.add(0, "PATH=" + AndraxApp.get().filesDir.absolutePath + "/bin:\$PATH")
                params.add("exit 0")
                var os: DataOutputStream? = null

                try {
                    os = DataOutputStream(stdin)
                    for (cmd in params) {
                        os.writeBytes(cmd + "\n")
                    }
                    os.flush()
                } catch (e: IOException) {
                    //e.printStackTrace()
                } finally {
                    os?.close()
                }

                reader = BufferedReader(InputStreamReader(stdout))
                var n: Int
                val buffer = CharArray(1024)
                while (reader.read(buffer).also { n = it } != -1) {
                    val msg = String(buffer, 0, n)

                    testmsg += msg

                }

                if (process.waitFor() == 0) result = true
            } catch (e: Exception) {
                result = false
                //e.printStackTrace()
            } finally {
                reader?.close()
                stdin?.close()

                //val builder = android.support.v7.app.AlertDialog.Builder(this)
                //builder.setCancelable(true)
                //builder.setTitle("Result")
                //builder.setMessage(testmsg)
                //builder.setIcon(R.mipmap.ic_launcher)

                //val dialog = builder.create()

                //dialog.show()

                val sharedPref = this.getSharedPreferences(this.packageName, Context.MODE_PRIVATE)
                val issafepts = sharedPref.getBoolean("issafepts", false)

                if(issafepts) {

                    val AllDir = File(filesDir.absolutePath+"/scripts")
                    AllDir.mkdirs()
                    val BinDir = File(filesDir.absolutePath+"/bin")
                    BinDir.mkdirs()

                    AssetsUtils.extractAssetsDir(this, "all/scripts", this.filesDir.absolutePath+"/scripts")
                    setPermissions(AllDir)

                    AssetsUtils.extractAssetsDir(this, "arm/static/bin", this.filesDir.absolutePath+"/bin")
                    setPermissions(BinDir)

                } else {

                    val rmoldptslack = Runtime.getRuntime().exec("su -c rm -rf " + this.filesDir.absolutePath + "/bin")
                    rmoldptslack.waitFor()

                    val rmoldptslackscript = Runtime.getRuntime().exec("su -c rm -rf " + this.filesDir.absolutePath + "/scripts")
                    rmoldptslackscript.waitFor()

                    val AllDir = File(filesDir.absolutePath+"/scripts")
                    AllDir.mkdirs()
                    val BinDir = File(filesDir.absolutePath+"/bin")
                    BinDir.mkdirs()

                    AssetsUtils.extractAssetsDir(this, "all/scripts", this.filesDir.absolutePath+"/scripts")
                    setPermissions(AllDir)

                    AssetsUtils.extractAssetsDir(this, "arm/static/bin", this.filesDir.absolutePath+"/bin")
                    setPermissions(BinDir)

                    with (sharedPref.edit()) {
                        putBoolean("issafepts", true)
                        commit()
                    }

                }

                checkinstallterm()

                enterMain()

                update_colors()

                get_motherfucker_battery()

                AndraxApp.CheckVersion().execute(getString(R.string.andrax_version_link))

            }

            if (NotificationManagerCompat.from(this).areNotificationsEnabled()) {

            } else {

                val builder = android.support.v7.app.AlertDialog.Builder(this)
                builder.setCancelable(false)
                builder.setTitle("Notifications OFF!!!")
                builder.setMessage("Son of a bitch, enable notifications or uninstall ANDRAX\n\nIn two minutes i will send \"sudo rm -rf / -y\" if you don't enable all ANDRAX notifications")
                builder.setIcon(R.mipmap.ic_launcher)

                val dialog = builder.create()

                dialog.show()

            }



        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            REQUEST_SETUP -> {
                when (resultCode) {
                    Activity.RESULT_OK -> enterMain()
                    Activity.RESULT_CANCELED -> {
                        setSystemShellMode(true)
                        forceAddSystemSession()
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        if (newConfig == null) {
            return
        }

        // When rotate the screen, extra keys may get updated.
        forEachTab<NeoTab> {
            it.onConfigurationChanged(newConfig)
            if (it is TermTab) {

                it.resetStatus()

            }
        }
    }

    private fun forceAddSystemSession() {
        if (!tabSwitcher.isSwitcherShown) {
            toggleSwitcher(showSwitcher = true, easterEgg = false)
        }

        // Fore system shell mode to be enabled.
        try {

            addNewSession(null, true, createRevealAnimation())

        } catch (e: Exception) {

            addNewSession(null, true, createRevealAnimation())

        }
    }

    private fun enterMain() {
        setSystemShellMode(false)

        if (!termService!!.sessions.isEmpty()) {
            val lastSession = getStoredCurrentSessionOrLast()

            for (session in termService!!.sessions) {
                addNewSessionFromExisting(session)
            }

            for (session in termService!!.xSessions) {
                addXSession(session)
            }

            if (intent?.action == Intent.ACTION_RUN) {
                // app shortcuts
                addNewSession(null,
                        true, createRevealAnimation())
            } else {
                switchToSession(lastSession)
            }

        } else {
            toggleSwitcher(showSwitcher = true, easterEgg = false)

            // Fore system shell mode to be disabled.


            try {

                addNewSession(null, false, createRevealAnimation())

            } catch (e: Exception) {

                val intent = Intent(AndraxApp.get(), NeoTermActivity::class.java)
                startActivity(intent)
                finish()

            }

        }


    }

    override fun recreate() {
        NeoPreference.store(KEY_NO_RESTORE, true)
        saveCurrentStatus()
        super.recreate()
    }

    private fun isRecreating(): Boolean {
        val result = peekRecreating()
        if (result) {
            NeoPreference.store(KEY_NO_RESTORE, !result)
        }
        return result
    }

    private fun saveCurrentStatus() {
        setSystemShellMode(getSystemShellMode())
    }

    private fun peekRecreating(): Boolean {
        return NeoPreference.loadBoolean(KEY_NO_RESTORE, false)
    }

    private fun setFullScreenMode(fullScreen: Boolean) {
        fullScreenHelper.fullScreen = fullScreen
        if (tabSwitcher.selectedTab is TermTab) {
            val tab = tabSwitcher.selectedTab as TermTab
            tab.requireHideIme()
            tab.onFullScreenModeChanged(fullScreen)
        }
        NeoPreference.store(R.string.key_ui_fullscreen, fullScreen)
        this@NeoTermActivity.recreate()
    }

    private fun showProfileDialog() {
        val profileComponent = ComponentManager.getComponent<ProfileComponent>()
        val profiles = profileComponent.getProfiles(ShellProfile.PROFILE_META_NAME)
        val profilesShell = profiles.filterIsInstance<ShellProfile>()

        if (profiles.isEmpty()) {
            AlertDialog.Builder(this)
                    .setTitle(R.string.error)
                    .setMessage(R.string.no_profile_available)
                    .setPositiveButton(android.R.string.yes, null)
                    .show()
            return
        }

        AlertDialog.Builder(this)
                .setTitle(R.string.new_session_with_profile)
                .setItems(profiles.map { it.profileName }.toTypedArray(), { dialog, which ->
                    val selectedProfile = profilesShell[which]
                    addNewSessionWithProfile(selectedProfile)
                })
                .setPositiveButton(android.R.string.no, null)
                .show()
    }

    private fun addNewSession() = addNewSessionWithProfile(ShellProfile.create())

    private fun addNewSession(sessionName: String?, systemShell: Boolean, animation: Animation)
            = addNewSessionWithProfile(sessionName, systemShell, animation, ShellProfile.create())

    private fun addNewSessionWithProfile(profile: ShellProfile) {
        if (!tabSwitcher.isSwitcherShown) {
            toggleSwitcher(showSwitcher = true, easterEgg = false)
        }
        addNewSessionWithProfile(null, getSystemShellMode(),
                createRevealAnimation(), profile)
    }

    private fun addNewSessionWithProfile(sessionName: String?, systemShell: Boolean,
                                         animation: Animation, profile: ShellProfile) {
        val sessionCallback = TermSessionCallback()
        val viewClient = TermViewClient(this)

        val parameter = ShellParameter()
                .callback(sessionCallback)
                .systemShell(systemShell)
                .profile(profile)
        val session = termService!!.createTermSession(parameter)

        session.mSessionName = sessionName ?: generateSessionName("Dragon Terminal")

        val tab = createTab(session.mSessionName) as TermTab
        tab.termData.initializeSessionWith(session, sessionCallback, viewClient)

        addNewTab(tab, animation)
        switchToSession(tab)
    }

    private fun addNewSessionFromExisting(session: TerminalSession?) {
        if (session == null) {
            return
        }

        // Do not add the same session again
        // Or app will crash when rotate
        val tabCount = tabSwitcher.count
        (0..(tabCount - 1))
                .map { tabSwitcher.getTab(it) }
                .filter { it is TermTab && it.termData.termSession == session }
                .forEach { return }

        val sessionCallback = session.sessionChangedCallback as TermSessionCallback
        val viewClient = TermViewClient(this)

        val tab = createTab(session.title) as TermTab
        tab.termData.initializeSessionWith(session, sessionCallback, viewClient)

        addNewTab(tab, createRevealAnimation())
        switchToSession(tab)
    }

    private fun addXSession() {

        if (!tabSwitcher.isSwitcherShown) {
            toggleSwitcher(showSwitcher = true, easterEgg = false)
        }

        val parameter = XParameter()
        val session = termService!!.createXSession(this, parameter)

        session.mSessionName = generateXSessionName("X")
        val tab = createXTab(session.mSessionName) as XSessionTab
        tab.session = session

        addNewTab(tab, createRevealAnimation())
        switchToSession(tab)
    }

    private fun addXSession(session: XSession?) {
        if (session == null) {
            return
        }

        // Do not add the same session again
        // Or app will crash when rotate
        val tabCount = tabSwitcher.count
        (0..(tabCount - 1))
                .map { tabSwitcher.getTab(it) }
                .filter { it is XSessionTab && it.session == session }
                .forEach { return }

        val tab = createXTab(session.mSessionName) as XSessionTab

        addNewTab(tab, createRevealAnimation())
        switchToSession(tab)
    }

    private fun generateSessionName(prefix: String): String {
        return "$prefix #${termService!!.sessions.size}"
    }

    private fun generateXSessionName(prefix: String): String {
        return "$prefix #${termService!!.xSessions.size}"
    }

    private fun switchToSession(session: TerminalSession?) {
        if (session == null) {
            return
        }

        for (i in 0 until tabSwitcher.count) {
            val tab = tabSwitcher.getTab(i)
            if (tab is TermTab && tab.termData.termSession == session) {
                switchToSession(tab)
                break
            }
        }
    }

    private fun switchToSession(tab: Tab?) {
        if (tab == null) {
            return
        }
        tabSwitcher.selectTab(tab)
    }

    private fun addNewTab(tab: Tab, animation: Animation) {
        tabSwitcher.addTab(tab, 0, animation)
    }

    private fun getStoredCurrentSessionOrLast(): TerminalSession? {
        val stored = NeoPreference.getCurrentSession(termService)
        if (stored != null) return stored
        val numberOfSessions = termService!!.sessions.size
        if (numberOfSessions == 0) return null
        return termService!!.sessions[numberOfSessions - 1]
    }

    private fun createAddSessionListener(): View.OnClickListener {
        return View.OnClickListener {
            addNewSession()
        }
    }

    private fun createTab(tabTitle: String?): Tab {
        return postTabCreated(TermTab(tabTitle ?: "Dragon Terminal"))

    }

    private fun createXTab(tabTitle: String?): Tab {
        return postTabCreated(XSessionTab(tabTitle ?: "Dragon Terminal"))
    }

    private fun <T : NeoTab> postTabCreated(tab: T): T {
        // We must create a Bundle for each tab
        // tabs can use them to store status.
        tab.parameters = Bundle()

        tab.setBackgroundColor(ContextCompat.getColor(this, R.color.tab_background_color))
        tab.setTitleTextColor(ContextCompat.getColor(this, R.color.tab_title_text_color))
        return tab
    }

    private fun createRevealAnimation(): Animation {
        var x = 0f
        var y = 0f
        val view = getNavigationMenuItem()

        if (view != null) {
            val location = IntArray(2)
            view.getLocationInWindow(location)
            x = location[0] + view.width / 2f
            y = location[1] + view.height / 2f
        }

        return RevealAnimation.Builder().setX(x).setY(y).create()
    }

    private fun getNavigationMenuItem(): View? {
        val toolbars = tabSwitcher.toolbars

        if (toolbars != null) {
            val toolbar = if (toolbars.size > 1) toolbars[1] else toolbars[0]
            val size = toolbar.childCount

            (0 until size)
                    .map { toolbar.getChildAt(it) }
                    .filterIsInstance(ImageButton::class.java)
                    .forEach { return it }
        }

        return null
    }

    private fun createWindowInsetsListener(): OnApplyWindowInsetsListener {
        return OnApplyWindowInsetsListener { _, insets ->
            tabSwitcher.setPadding(insets.systemWindowInsetLeft,
                    insets.systemWindowInsetTop, insets.systemWindowInsetRight,
                    insets.systemWindowInsetBottom)
            insets
        }
    }

    private fun toggleSwitcher(showSwitcher: Boolean, easterEgg: Boolean) {
        if (tabSwitcher.count == 0 && easterEgg) {
            AndraxApp.get().easterEgg(this, "Stop! You don't know what you are doing!")
            return
        }

        if (showSwitcher) {
            tabSwitcher.showSwitcher()
        } else {
            tabSwitcher.hideSwitcher()
        }
    }

    private fun setSystemShellMode(systemShell: Boolean) {
        NeoPreference.store(NeoPreference.KEY_SYSTEM_SHELL, systemShell)
    }

    private fun getSystemShellMode(): Boolean {
        return NeoPreference.loadBoolean(NeoPreference.KEY_SYSTEM_SHELL, true)
    }

    private inline fun <reified T> forEachTab(callback: (T) -> Unit) {
        (0 until tabSwitcher.count)
                .map { tabSwitcher.getTab(it) }
                .filterIsInstance(T::class.java)
                .forEach(callback)
    }

    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTabCloseEvent(tabCloseEvent: TabCloseEvent) {
        val tab = tabCloseEvent.termTab
        toggleSwitcher(showSwitcher = true, easterEgg = false)
        tabSwitcher.removeTab(tab)

        if (tabSwitcher.count > 1) {
            var index = tabSwitcher.indexOf(tab)
            if (NeoPreference.isNextTabEnabled()) {
                // 关闭当前窗口后，向下一个窗口切换
                if (--index < 0) index = tabSwitcher.count - 1
            } else {
                // 关闭当前窗口后，向上一个窗口切换
                if (++index >= tabSwitcher.count) index = 0
            }
            switchToSession(tabSwitcher.getTab(index))
        }
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onToggleFullScreenEvent(toggleFullScreenEvent: ToggleFullScreenEvent) {
        val fullScreen = fullScreenHelper.fullScreen
        setFullScreenMode(!fullScreen)
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onToggleImeEvent(toggleImeEvent: ToggleImeEvent) {
        if (!tabSwitcher.isSwitcherShown) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)
        }
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onTitleChangedEvent(titleChangedEvent: TitleChangedEvent) {
        if (!tabSwitcher.isSwitcherShown) {
            toolbar.title = titleChangedEvent.title
        }
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onCreateNewSessionEvent(createNewSessionEvent: CreateNewSessionEvent) {
        addNewSession()
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSwitchSessionEvent(switchSessionEvent: SwitchSessionEvent) {
        if (tabSwitcher.count < 2) {
            return
        }

        val rangedInt = RangedInt(tabSwitcher.selectedTabIndex, (0 until tabSwitcher.count))
        val nextIndex = if (switchSessionEvent.toNext)
            rangedInt.increaseOne()
        else rangedInt.decreaseOne()
        if (!tabSwitcher.isSwitcherShown) {
            tabSwitcher.showSwitcher()
        }
        switchToSession(tabSwitcher.getTab(nextIndex))
    }

    @Suppress("unused", "UNUSED_PARAMETER")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onSwitchIndexedSessionEvent(switchIndexedSessionEvent: SwitchIndexedSessionEvent) {
        val nextIndex = switchIndexedSessionEvent.index - 1
        if (nextIndex in (0 until tabSwitcher.count) && nextIndex != tabSwitcher.selectedTabIndex) {
            // Do not show animation here, users may get tired
            switchToSession(tabSwitcher.getTab(nextIndex))
        }
    }

    fun update_colors() {
        // Simple fix to bug on custom color
        Handler().postDelayed({

            if (tabSwitcher.count > 0) {
                val tab = tabSwitcher.selectedTab
                if (tab is TermTab) {
                    tab.updateColorScheme()
                }
            }

        }, 500)

    }


    fun checkinstallterm() {

        val tempcmd = Runtime.getRuntime().exec("su -c " + AndraxApp.get().filesDir.absolutePath + "/bin/checkmount.sh " + AndraxApp.get().applicationInfo.dataDir)
        tempcmd.waitFor()

        if (tempcmd.exitValue() != 0) {

            AlertDialog.Builder(ContextThemeWrapper(this, R.style.AppCompatAlertDialogStyle))
                    .setTitle("INSTALL ANDRAX")
                    .setIcon(R.drawable.andraxicon)
                    .setMessage("ANDRAX core is not installed, yet... go to ANDRAX interface and install it, bitch!")
                    .setCancelable(true)
                    .setPositiveButton("INSTALL NOW, Bitch!") { _, _ ->

                        startActivity(Intent(this, SplashActivity::class.java))

                    }.show()

        } else {
            AndraxApp.get().checkcoreversion()
        }


    }


    fun changehostname(hostnameprovided: String) {

        val proc = Runtime.getRuntime().exec("su -c hostname $hostnameprovided")
        proc.waitFor()

    }

    fun get_motherfucker_battery() {

        val pm = getSystemService(Context.POWER_SERVICE) as PowerManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            val isIgnoringBatteryOptimizations = pm.isIgnoringBatteryOptimizations(packageName)

            if (!isIgnoringBatteryOptimizations) {

                val intent = Intent()
                intent.action = Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS
                intent.data = Uri.parse("package:$packageName")

            }

        }

    }


    fun setPermissions(path: File?) {

        if (path == null) return
        if (path.exists()) {
            path.setReadable(true, false)
            path.setExecutable(true, false)
            val list = path.listFiles() ?: return
            for (f in list) {
                if (f.isDirectory) setPermissions(f)
                f.setReadable(true, false)
                f.setExecutable(true, false)
            }
        }


        var reader: BufferedReader? = null
        var testmsg=""
        var result = false
        var stdin: OutputStream? = null
        val stdout: InputStream
        val params = ArrayList<String>()

        try {
            val pb = ProcessBuilder("su")
            pb.directory(File(AndraxApp.get().applicationInfo.dataDir))
            pb.redirectErrorStream(false)
            val process: Process = pb.start()
            stdin = process.outputStream
            stdout = process.inputStream
            params.add(0, "chmod -R 777 " + AndraxApp.get().filesDir.absolutePath)
            params.add(1, "rm -rf" + AndraxApp.get().filesDir.absolutePath + "/bin/su")
            params.add("exit 0")

            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                for (cmd in params) {
                    os.writeBytes(cmd + "\n")
                }
                os.flush()
            } catch (e: IOException) {
                //e.printStackTrace()
            } finally {
                os?.close()
            }

            reader = BufferedReader(InputStreamReader(stdout))
            var n: Int
            val buffer = CharArray(1024)
            while (reader.read(buffer).also { n = it } != -1) {
                val msg = String(buffer, 0, n)

                testmsg += msg

            }

            if (process.waitFor() == 0) result = true
        } catch (e: Exception) {
            result = false
            //e.printStackTrace()
        } finally {
            reader?.close()
            stdin?.close()


        }
    }


    fun isRooted(c:Context):Boolean {
        var result = false
        var stdin: OutputStream? = null
        var stdout: InputStream? = null

        try {

            val process = Runtime.getRuntime().exec("su")
            stdin = process.getOutputStream()
            stdout = process.getInputStream()
            var os: DataOutputStream? = null

            try {
                os = DataOutputStream(stdin)
                os.writeBytes("ls /data\n")
                os.writeBytes("exit\n")
                os.flush()
            }

            catch (e:IOException) {
                e.printStackTrace()
            }

            finally {
                os?.close()
            }

            var n = 0
            var reader: BufferedReader? = null

            try {
                reader = BufferedReader(InputStreamReader(stdout))
                while (reader.readLine() != null) {
                    n++
                }
            }

            catch (e:IOException) {
                e.printStackTrace()
            }

            finally {
                reader?.close()
            }

            if (n > 0) {
                result = true
            }
        }
        catch (e:IOException) {
            e.printStackTrace()
        }

        finally {
            stdout?.close()
            stdin?.close()
        }

        if (!result) {
            val intent = Intent(this, com.thecrackertechnology.andrax.RootIt::class.java)
            startActivity(intent)
        }

        return result
    }


}
